package com.vote.business.calculator;

import com.vote.business.model.HcgConfiguration;
import com.vote.business.model.HcgData;
import com.vote.business.model.HcgRange;
import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.joda.time.Period;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class BetaHcgCalculator {

    public BigDecimal calculate(HcgData hcgData)
    {
        DateTime from = hcgData.getFirstDate();
        DateTime to = hcgData.getSecondDate();
        BigDecimal first = hcgData.getFirstValue();
        BigDecimal second = hcgData.getSecondValue();
        Period diff = new Period(from,to);
        String period = diff.toStandardHours().toString();
        Hours hours =  diff.toStandardHours();
        BigDecimal ratio = second.subtract(first);
        ratio = ratio.divide(first,2,RoundingMode.HALF_UP);
        return ratio;
    }
    public Boolean isNormal(HcgData hcgData)
    {
        for (HcgRange range : HcgConfiguration.getHcgRanges())
        {
            if (range.getBetaHcgRange().contains(hcgData.getFirstValue()))
            {
                if(range.getDoublingTimeRange().contains(hcgData.getDoublingTime()))
                {
                    return true;
                }
            }
        }
        return false;
    }
}

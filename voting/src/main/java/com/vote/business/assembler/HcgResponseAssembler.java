package com.vote.business.assembler;

import com.vote.business.model.HcgResponseDto;
import java.math.BigDecimal;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class HcgResponseAssembler {
    public static HcgResponseDto assembleHcgResponseDto(BigDecimal doublingTime, BigDecimal totalIncrease, Boolean isNormal){
        HcgResponseDto dto = new HcgResponseDto(doublingTime,totalIncrease,isNormal);
        return dto;
    }
}

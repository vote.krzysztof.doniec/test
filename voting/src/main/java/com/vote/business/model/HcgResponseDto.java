package com.vote.business.model;

import java.math.BigDecimal;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class HcgResponseDto
{
    private BigDecimal doublingTime;
    private BigDecimal totalIncrease;
    private Boolean isNormal;

    public BigDecimal getDoublingTime() {
        return doublingTime;
    }

    public void setDoublingTime(BigDecimal doublingTime) {
        this.doublingTime = doublingTime;
    }

    public BigDecimal getTotalIncrease() {
        return totalIncrease;
    }

    public void setTotalIncrease(BigDecimal totalIncrease) {
        this.totalIncrease = totalIncrease;
    }

    public Boolean getIsNormal() {
        return isNormal;
    }

    public void setIsNormal(Boolean isNormal) {
        this.isNormal = isNormal;
    }

    public HcgResponseDto(BigDecimal doublingTime, BigDecimal totalIncrease, Boolean isNormal) {
        this.doublingTime = doublingTime;
        this.totalIncrease = totalIncrease;
        this.isNormal = isNormal;
    }
}

package com.vote.business.model;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class HcgRange {
    private Range betaHcgRange;
    private Range doublingTimeRange;

    public HcgRange(Range betaHcgRange, Range doublingTimeRange) {
        this.betaHcgRange = betaHcgRange;
        this.doublingTimeRange = doublingTimeRange;
    }

    public Boolean isInRange(HcgData hcgData)
    {
        Boolean check = betaHcgRange.contains(hcgData.getFirstValue())&&doublingTimeRange.contains(hcgData.getDoublingTime());
        return check;
    }

    public Range getBetaHcgRange() {
        return betaHcgRange;
    }

    public Range getDoublingTimeRange() {
        return doublingTimeRange;
    }
}

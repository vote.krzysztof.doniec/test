package com.vote.business.model;

import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.joda.time.Period;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class HcgData {
    private BigDecimal firstValue;
    private BigDecimal secondValue;
    private DateTime firstDate;
    private DateTime secondDate;

    public HcgData(BigDecimal firstValue, BigDecimal secondValue, DateTime firstDate, DateTime secondDate) {
        this.firstValue = firstValue;
        this.secondValue = secondValue;
        this.firstDate = firstDate;
        this.secondDate = secondDate;
    }

    public BigDecimal getFirstValue() {
        return firstValue;
    }

    public BigDecimal getSecondValue() {
        return secondValue;
    }

    public DateTime getFirstDate() {
        return firstDate;
    }

    public DateTime getSecondDate() {
        return secondDate;
    }

    public BigDecimal getHours()
    {
        Period diff = new Period(firstDate,secondDate);
        Hours hours = diff.toStandardHours();
        BigDecimal timeDifference = BigDecimal.valueOf(hours.getValue(0));
        return timeDifference;
    };

    public BigDecimal getDoublingTime()
    {
        BigDecimal value = getHours().multiply(new BigDecimal("2"));
        BigDecimal totalIncrease = secondValue.divide(firstValue,2,
                                                      RoundingMode.HALF_UP);
        BigDecimal result = value.divide(totalIncrease,2,
                                         RoundingMode.HALF_UP);
        return result;
    }
}

package com.vote.business.model;

import java.math.BigDecimal;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class Range {
    private BigDecimal from;
    private BigDecimal to;

    public Range(BigDecimal from, BigDecimal to) {
        this.from = from;
        this.to = to;
    }

    public BigDecimal getFrom() {
        return from;
    }

    public BigDecimal getTo() {
        return to;
    }

    public Boolean isBelowTo(BigDecimal value)
    {
        return to==null?Boolean.TRUE:value.compareTo(to)==1?Boolean.FALSE:Boolean.TRUE;
    }
    public Boolean isAboveFrom(BigDecimal value)
    {
        return value.compareTo(from)==1?Boolean.TRUE:Boolean.FALSE;
    }
    public Boolean contains (BigDecimal value) {

        return isBelowTo(value)&&isAboveFrom(value);
    }
}

package com.vote.business.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class HcgDataInDto implements Serializable{
    private BigDecimal first;
    private BigDecimal second;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private DateTime firstDate;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private DateTime secondDate;

    public BigDecimal getFirst() {
        return first;
    }

    public void setFirst(BigDecimal first) {
        this.first = first;
    }

    public BigDecimal getSecond() {
        return second;
    }

    public void setSecond(BigDecimal second) {
        this.second = second;
    }

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    public DateTime getFirstDate() {
        return firstDate;
    }

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    public void setFirstDate(DateTime firstDate) {
        this.firstDate = firstDate;
    }

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    public DateTime getSecondDate() {
        return secondDate;
    }

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    public void setSecondDate(DateTime secondDate) {
        this.secondDate = secondDate;
    }
}

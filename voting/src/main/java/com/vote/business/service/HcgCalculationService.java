package com.vote.business.service;

import com.vote.business.calculator.BetaHcgCalculator;
import com.vote.business.model.HcgData;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;

/**
 * @author Krzysztof Doniec (A043986)
 */
@Service
public class HcgCalculationService {

    public BigDecimal totalIncrease(HcgData hcgData)
    {
        BetaHcgCalculator calculator = new BetaHcgCalculator();
        BigDecimal totalIncrease = calculator.calculate(hcgData).multiply(new BigDecimal("100"));
        return totalIncrease;
    }

    public BigDecimal doublingTime(HcgData hcgData)
    {
        return hcgData.getDoublingTime();
    }

    public Boolean isNormal(HcgData data)
    {
        BetaHcgCalculator calculator = new BetaHcgCalculator();
        return calculator.isNormal(data);
    }

}

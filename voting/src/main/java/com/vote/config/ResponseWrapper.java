package com.vote.config;

/**
 * @author Dawid Kotarba (A041202)
 */

/**
 * This class is a wrapper for all primitives that need to be returned by controllers to front-end.
 *
 * @param <T>
 */
public class ResponseWrapper<T> {

    private T response;

    public ResponseWrapper(T response) {
        this.response = response;
    }

    public T getResponse() {
        return response;
    }
}

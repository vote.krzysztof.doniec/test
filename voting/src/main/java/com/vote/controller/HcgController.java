package com.vote.controller;

import com.vote.business.assembler.HcgResponseAssembler;
import com.vote.business.model.HcgData;
import com.vote.business.model.HcgDataInDto;
import com.vote.business.model.HcgResponseDto;
import com.vote.business.service.HcgCalculationService;
import com.vote.config.ResponseWrapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
/**
 * @author Krzysztof Doniec (A043986)
 */
@RestController
public class HcgController {

    private final HcgCalculationService hcgCalculationService;

    @Autowired
    public HcgController(HcgCalculationService hcgCalculationService) {
        this.hcgCalculationService = hcgCalculationService;
    }

    @RequestMapping(value="/hcg/{First}/{Second}",method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper<HcgResponseDto>> getTotalIncrease(@PathVariable("First") BigDecimal first,@PathVariable("Second") BigDecimal second) {//Welco// e page, non-rest
        HcgData hcgData = new HcgData(first,
                                      second,
                                      new DateTime(2005, 3, 26, 10, 0, 0, 0),
                                      new DateTime(2005, 3, 27, 10, 0, 0, 0));
        BigDecimal totalIncrease = hcgCalculationService.totalIncrease(hcgData);
        BigDecimal doublingTime = hcgCalculationService.doublingTime(hcgData);
        Boolean isNormal = hcgCalculationService.isNormal(hcgData);
        HcgResponseDto dto = HcgResponseAssembler.assembleHcgResponseDto(doublingTime,totalIncrease,isNormal);
        ResponseWrapper<HcgResponseDto> wrapper = new ResponseWrapper<HcgResponseDto>(dto);
        //return ResponseEntity.ok().body(wrapper);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        ResponseEntity<ResponseWrapper<HcgResponseDto>> re = new ResponseEntity<ResponseWrapper<HcgResponseDto>>(wrapper, headers, HttpStatus.OK);
        return re;
    }
    @RequestMapping(value="/xyz",method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
    consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper<HcgResponseDto>> getHcgInfo(@RequestBody HcgDataInDto test) {//Welco// e page, non-rest
        HcgData hcgData = new HcgData(test.getFirst(),
                                      test.getSecond(),
                                      test.getFirstDate(),
                                      test.getSecondDate());
        BigDecimal totalIncrease = hcgCalculationService.totalIncrease(hcgData);
        BigDecimal doublingTime = hcgCalculationService.doublingTime(hcgData);
        Boolean isNormal = hcgCalculationService.isNormal(hcgData);
        HcgResponseDto dto = HcgResponseAssembler.assembleHcgResponseDto(doublingTime,totalIncrease,isNormal);
        ResponseWrapper<HcgResponseDto> wrapper = new ResponseWrapper<HcgResponseDto>(dto);
        //return ResponseEntity.ok().body(wrapper);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        ResponseEntity<ResponseWrapper<HcgResponseDto>> re = new ResponseEntity<ResponseWrapper<HcgResponseDto>>(wrapper, headers, HttpStatus.OK);
        return re;
    }
    @RequestMapping(value="/method0")
    @ResponseBody
    public HcgResponseDto method0(){
        HcgData hcgData = new HcgData(new BigDecimal("2"),
                                      new BigDecimal("3"),
                                      new DateTime(2005, 3, 26, 10, 0, 0, 0),
                                      new DateTime(2005, 3, 27, 10, 0, 0, 0));
        BigDecimal totalIncrease = hcgCalculationService.totalIncrease(hcgData);
        BigDecimal doublingTime = hcgCalculationService.doublingTime(hcgData);
        Boolean isNormal = hcgCalculationService.isNormal(hcgData);
        HcgResponseDto dto = HcgResponseAssembler.assembleHcgResponseDto(doublingTime,totalIncrease,isNormal);
        return dto;
    }
}

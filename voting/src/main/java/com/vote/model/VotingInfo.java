package com.vote.model;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class VotingInfo {
    private Date date;
    private ArrayList<String> parties;

    public VotingInfo(Date date, ArrayList<String> parties) {
        this.date = date;
        this.parties = parties;
    }

    public Date getDate() {

        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<String> getParties() {
        return parties;
    }

    public void setParties(ArrayList<String> parties) {
        this.parties = parties;
    }
}

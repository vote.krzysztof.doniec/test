var app = angular.module("demo", []);
    app.controller("HttpGetController", function ($scope, $http) {
        $(function() {

            //for displaying datepicker

            $('#datetimepicker').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY HH:mm'
            });

            //for getting input value

            $("#datetimepicker").on("dp.change", function() {

                $scope.selecteddate = $("#datetimepicker").val();

            });

            $('#datetimepickerSecond').datetimepicker({
                viewMode: 'years',
                format: 'DD-MM-YYYY HH:mm'
            });
            $("#datetimepickerSecond").on("dp.change", function() {

                $scope.secondDate = $("#datetimepickerSecond").val();

            });
        });

		var x = 3;
		$.support.cors = true;
        $scope.SendData = function () {
           // use $.param jQuery function to serialize data from JSON
            var data =  {
                "first": $scope.first,
                "second": $scope.second,
                "firstDate": $scope.selecteddate,
                "secondDate" : $scope.secondDate
            };
        
            var config = {
                headers : {
                    'Content-Type': 'application/json'
                }
            }
            $http.post('/xyz', data, config)
            .success(function (data, status, headers, config) {
                $scope.PostDataResponse = data;
            })
            .error(function (data, status, header, config) {
                $scope.ResponseDetails = "Data: " + data +
                    "<hr />status: " + status +
                    "<hr />headers: " + header +
                    "<hr />config: " + config;
            });
        };

    });


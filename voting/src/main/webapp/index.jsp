
<!DOCTYPE html>
<html ng-app="demo">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.min.js"></script>
    <script src="/resources/hello.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
</head>
<body>

<div class="container">
    <form>
        <div class="form-group">
            <label for="first">First Beta:</label>
            <input type="text" class="form-control"  ng-model="first" id="first" placeholder="First Beta">
        </div>
        <div class='input-group date' id='datetimepicker1' >
            <input class="form-control" name="date" id="datetimepicker" placeholder="select date">
        </div>
        <div class="form-group">
            <label for="second">Second Beta:</label>
            <input type="text" class="form-control" ng-model="second" id="second" placeholder="Second Beta">
        </div>
        <div class='input-group date' id='datetimepicker2' >
            <input class="form-control" name="dateSecond" id="datetimepickerSecond" placeholder="select date">
        </div>
        <div ng-controller="HttpGetController">
            <br>
            <button ng-click="SendData()">Submit</button>
            <p>Your results</p>
            <br>
            <p>Doubling time: {{PostDataResponse.response.doublingTime}}</p>
            <p>Total Increase: {{PostDataResponse.response.totalIncrease}}[%]</p>
        </div>
    </form>
</div>


</body>
</html>

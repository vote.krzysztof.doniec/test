package com.betahcg.model;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.*;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class HcgDataTest {

    @Test
    public void testGetHours() throws Exception {
        HcgData hcgData = new HcgData(new BigDecimal("100"),
                                      new BigDecimal("300"),
                                      new DateTime(2005, 3, 26, 10, 0, 0, 0),
                                      new DateTime(2005, 3, 27, 10, 0, 0, 0));
        assertEquals(hcgData.getHours(),new BigDecimal("24"));
    }

    @Test
    public void testGetDoublingTime() throws Exception {
        HcgData hcgData = new HcgData(new BigDecimal("100"),
                                      new BigDecimal("200"),
                                      new DateTime(2005, 3, 26, 10, 0, 0, 0),
                                      new DateTime(2005, 3, 27, 10, 0, 0, 0));
        assertEquals(hcgData.getDoublingTime(),new BigDecimal("24"));
    }
}
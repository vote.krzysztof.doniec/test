package com.betahcg.model;

import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.*;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class RangeTest {

    @Test
    public void testContains() throws Exception {
        Range range = new Range(new BigDecimal("10"),new BigDecimal("20"));
        assertEquals(range.contains(new BigDecimal("15")),Boolean.TRUE);
        assertEquals(range.contains(new BigDecimal("25")),Boolean.FALSE);
        assertEquals(range.contains(new BigDecimal("5")),Boolean.FALSE);
    }
    @Test
    public void testContainsWithNull() throws Exception {
        Range range = new Range(new BigDecimal("10"),null);
        assertEquals(range.contains(new BigDecimal("15")),Boolean.TRUE);
        assertEquals(range.contains(new BigDecimal("25")),Boolean.TRUE);
        assertEquals(range.contains(new BigDecimal("5")),Boolean.FALSE);
        assertEquals(range.contains(new BigDecimal("1000")),Boolean.TRUE);
    }
}
package com.betahcg.model;

import org.joda.time.DateTime;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.*;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class HcgRangeTest {

    @Test
    public void testIsInRange() throws Exception {
        Range valueRange = new Range(new BigDecimal("1000"),new BigDecimal("2000"));
        Range timeRange = new Range(new BigDecimal("15"),new BigDecimal("20"));
        HcgRange hcgRange = new HcgRange(valueRange,timeRange);
        HcgData hcgData = new HcgData(new BigDecimal("1001"),
                                      new BigDecimal("3000"),
                                      new DateTime(2005, 3, 26, 10, 0, 0, 0),
                                      new DateTime(2005, 3, 27, 10, 0, 0, 0));
        BigDecimal time = hcgData.getDoublingTime();
        Boolean check = hcgRange.isInRange(hcgData);
        assertEquals(check,Boolean.TRUE);
    }
}
package com.betahcg.service;

import com.betahcg.calculator.BetaHcgCalculator;
import com.betahcg.model.HcgData;
import org.joda.time.DateTime;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.*;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class HcgCalculationServiceTest {

    HcgData hcgData;
    BetaHcgCalculator betaHcgCalculator;
    HcgCalculationService hcgCalculationService;
    @BeforeTest
    public void test()
    {
        HcgData hcgData = new HcgData(new BigDecimal("100"),
                                      new BigDecimal("300"),
                                      new DateTime(2005, 3, 26, 10, 0, 0, 0),
                                      new DateTime(2005, 3, 27, 10, 0, 0, 0));
        betaHcgCalculator = new BetaHcgCalculator();
        hcgCalculationService = new HcgCalculationService();
    }

    @Test
    public void testTotalIncrease() throws Exception {
        HcgData hcgData = new HcgData(new BigDecimal("100"),
                                      new BigDecimal("300"),
                                      new DateTime(2005, 3, 26, 10, 0, 0, 0),
                                      new DateTime(2005, 3, 27, 10, 0, 0, 0));
        betaHcgCalculator = new BetaHcgCalculator();
        hcgCalculationService = new HcgCalculationService();
        BigDecimal totalIncrease = hcgCalculationService.totalIncrease(hcgData);
        int a = 10;
    }

    @Test
    public void testDoublingTime() throws Exception {
        HcgData hcgData = new HcgData(new BigDecimal("100"),
                                      new BigDecimal("300"),
                                      new DateTime(2005, 3, 26, 10, 0, 0, 0),
                                      new DateTime(2005, 3, 27, 10, 0, 0, 0));
        betaHcgCalculator = new BetaHcgCalculator();
        hcgCalculationService = new HcgCalculationService();
        BigDecimal doublingTime = hcgCalculationService.doublingTime(hcgData);
        int a = 10;
    }

    @Test
    public void testIsNormal() throws Exception {
        HcgData hcgData = new HcgData(new BigDecimal("100"),
                                      new BigDecimal("300"),
                                      new DateTime(2005, 3, 26, 10, 0, 0, 0),
                                      new DateTime(2005, 3, 27, 10, 0, 0, 0));
        betaHcgCalculator = new BetaHcgCalculator();
        hcgCalculationService = new HcgCalculationService();
        Boolean isNormal = hcgCalculationService.isNormal(hcgData);
        int a = 10;
    }
}
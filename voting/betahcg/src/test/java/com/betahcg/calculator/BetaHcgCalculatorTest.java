package com.betahcg.calculator;

import junit.framework.TestCase;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.*;
import static org.testng.Assert.assertEquals;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class BetaHcgCalculatorTest extends TestCase {

    BetaHcgCalculator betaHcgCalculator;

    @Test
    public void testCalculate() throws Exception {
        betaHcgCalculator = new BetaHcgCalculator();
//        String result = betaHcgCalculator.calculate(new DateTime(2005, 3, 26, 10, 0, 0, 0),
//                                                    new DateTime(2005, 3, 27, 11, 0, 0, 0),
//                                                    new BigDecimal("100"),
//                                                    new BigDecimal("230"));
        assertEquals(BigDecimal.ONE, BigDecimal.TEN);

    }
}
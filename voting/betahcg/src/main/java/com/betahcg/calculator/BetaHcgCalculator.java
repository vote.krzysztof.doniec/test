package com.betahcg.calculator;
import com.betahcg.model.HcgConfiguration;
import com.betahcg.model.HcgData;
import com.betahcg.model.HcgRange;
import com.betahcg.model.Range;
import org.joda.time.*;
import java.math.BigDecimal;

import static java.math.BigDecimal.*;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class BetaHcgCalculator {

    public BigDecimal calculate(HcgData hcgData)
    {
        DateTime from = hcgData.getFirstDate();
        DateTime to = hcgData.getSecondDate();
        BigDecimal first = hcgData.getFirstValue();
        BigDecimal second = hcgData.getSecondValue();
        Period diff = new Period(from,to);
        String period = diff.toStandardHours().toString();
        Hours hours =  diff.toStandardHours();
        BigDecimal ratio = second.subtract(first).divide(first);
        return ratio;
    }
    public Boolean isNormal(HcgData hcgData)
    {
        for (HcgRange range : HcgConfiguration.getHcgRanges())
        {
            if (range.getBetaHcgRange().contains(hcgData.getFirstValue()))
            {
                if(range.getDoublingTimeRange().contains(hcgData.getDoublingTime()))
                {
                    return true;
                }
                else return false;
            }
            return false;
        }
        return false;
    }
}

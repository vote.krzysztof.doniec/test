package com.betahcg.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class HcgConfiguration {
    private static ArrayList<HcgRange> hcgRanges = new ArrayList<HcgRange>()
    {
        {
            add(new HcgRange(
                    new Range(BigDecimal.ZERO,new BigDecimal("1200")),
                              new Range(new BigDecimal("30"),new BigDecimal("72"))));
            new HcgRange(
                    new Range(new BigDecimal("1201"),new BigDecimal("6000")),
                    new Range(new BigDecimal("72"),new BigDecimal("96")));
            new HcgRange(
                    new Range(new BigDecimal("6001"),null),
                    new Range(new BigDecimal("96"),null));

        }
    };

    public static ArrayList<HcgRange> getHcgRanges() {
        return hcgRanges;
    }
}

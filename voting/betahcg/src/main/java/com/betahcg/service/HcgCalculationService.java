package com.betahcg.service;

import com.betahcg.calculator.BetaHcgCalculator;
import com.betahcg.model.HcgConfiguration;
import com.betahcg.model.HcgData;
import java.math.BigDecimal;

/**
 * @author Krzysztof Doniec (A043986)
 */
public class HcgCalculationService {

    public BigDecimal totalIncrease(HcgData hcgData)
    {
        BetaHcgCalculator calculator = new BetaHcgCalculator();
        BigDecimal totalIncrease = calculator.calculate(hcgData);
        return totalIncrease;
    }

    public BigDecimal doublingTime(HcgData hcgData)
    {
        return hcgData.getDoublingTime();
    }

    public Boolean isNormal(HcgData data)
    {
        BetaHcgCalculator calculator = new BetaHcgCalculator();
        return calculator.isNormal(data);
    }

}
